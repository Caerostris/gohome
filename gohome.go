/*
 * GoHome - A minimalist webserver for a personal homepage with a blog
 *
 * Copyright (c) 2014 Keno Schwalb
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 */

package main

import (
	"0x.cx/Caerostris/smtp"
	"encoding/base64"
	"fmt"
	"github.com/cloudflare/conf"
	"github.com/hoisie/mustache"
	flag "github.com/ogier/pflag"
	"labix.org/v2/mgo"
	"labix.org/v2/mgo/bson"
	"io/ioutil"
	"log"
	"math"
	"net/http"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"
)

type BlogArticle struct {
	Id         bson.ObjectId `bson:"_id,omitempty"`
	Title      string
	Author     string
	Content    string
	Date       time.Time
	HasGallery bool
	Gallery    []map[string]string
}

type PageList struct {
	PageMap map[string]bool
}

var pages *PageList
var blogAddress string
var smtpHost string
var cdnUrl string
var httpPort int
var smtpPort int

/* reusable mustache Templates */
var TmplIndex *mustache.Template
var TmplBlog *mustache.Template

/* database handle */
var c *mgo.Collection

/* query data required for the blog page */
func getBlogPage(pageNumber int) ([]map[string]interface{}, []map[string]interface{}, error) {
	var blogArticles []BlogArticle
	var postCount int
	blogArticleMap := make([]map[string]interface{}, 0)
	blogPageMap := []map[string]interface{}{}

	/* query database */
	err := c.Find(bson.M{}).Sort("-date").Skip((pageNumber - 1) * 5).Limit(5).All(&blogArticles)
	if err != nil {
		return nil, nil, err
	}

	/* build the blogArticles structure */
	for i := 0; i < len(blogArticles); i++ {
		m := make(map[string]interface{})
		m["blog_author"] = blogArticles[i].Author
		m["blog_title"] = blogArticles[i].Title
		m["blog_content"] = blogArticles[i].Content
		m["_id"] = blogArticles[i].Id.Hex()
		m["has_gallery"] = blogArticles[i].HasGallery
		blogArticleMap = append(blogArticleMap, m)
	}

	postCount, err = c.Count()
	if err != nil {
		return nil, nil, err
	}

	/* count pages and build the navigation structure */
	for i := 1; i <= int(math.Ceil(float64(postCount)/float64(5))); i++ {
		m := make(map[string]interface{})
		if i == pageNumber {
			m["page_active"] = true
		} else {
			m["page_active"] = false
		}
		m["page_number"] = strconv.Itoa(i)
		blogPageMap = append(blogPageMap, m)
	}

	return blogArticleMap, blogPageMap, nil
}

/* serve generic pages */
func indexHandler(w http.ResponseWriter, r *http.Request) {
	var err error
	var pageTitle string
	var archivePage string
	pageActive := "home" // default page is "home"
	blogNavigationPage := 1

	/* set route */
	if strings.HasPrefix(r.URL.Path, "/blog/archive/") {
		pageActive = "blog"
		blogNavigationPage, err = strconv.Atoi(r.URL.Path[14:])
		archivePage = r.URL.Path[5:]
		if err != nil {
			fmt.Fprintf(w, "invalid argument")
			return
		}
	} else if len(r.URL.Path) > 1 {
		pageActive = r.URL.Path[1:]
	}

	if pages.Exists(pageActive) {
		pageTitle = " » " + strings.ToUpper(pageActive[0:1]) + pageActive[1:]
	} else {
		notFound(w, r)
		return
	}

	/* construct mustache view */
	blogArticles, blogNavigation, err := getBlogPage(blogNavigationPage)

	if(err != nil) {
		notFound(w, r)
		return
	}

	muView := map[string]interface{}{}
	muView[pageActive+"_active"] = "active"
	muView["blog_articles"] = blogArticles
	muView["blog_pages"] = blogNavigation
	muView["archive_page"] = archivePage
	muView["page_title"] = pageTitle
	muView["cdn_url"] = cdnUrl
	fmt.Fprintf(w, "%s", TmplIndex.Render(muView))
}

/* serve blog articles */
func blogHandler(w http.ResponseWriter, r *http.Request) {
	/* validate parameter */
	if !bson.IsObjectIdHex(r.URL.Path[11:]) {
		fmt.Fprintf(w, "invalid ObjectId")
		return
	}

	/* query database */
	blogPost := BlogArticle{}
	err := c.Find(bson.M{"_id": bson.ObjectIdHex(r.URL.Path[11:])}).One(&blogPost)
	if err == mgo.ErrNotFound {
		notFound(w, r)
		return
	} else if err != nil {
		log.Println(err)
		notFound(w, r)
		return
	}

	/* construct mustache view */
	blog := make(map[string]interface{})
	blog["blog_active"] = "active"
	blog["blog_author"] = blogPost.Author
	blog["blog_title"] = blogPost.Title
	blog["blog_content"] = blogPost.Content
	blog["blog_datetime"] = blogPost.Date.Format(time.RFC3339)
	blog["blog_date"] = blogPost.Date.Format("02 Jan 2006")
	blog["has_gallery"] = blogPost.HasGallery
	blog["gallery"] = blogPost.Gallery
	blog["cdn_url"] = cdnUrl

	/* write response */
	fmt.Fprintf(w, "%s", TmplBlog.Render(blog))
}

func notFound(w http.ResponseWriter, r *http.Request) {
	http.Error(w, "404 - All kitties gone", 404)
}

func addCacheHeader(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Expires", time.Now().AddDate(60, 0, 0).Format(http.TimeFormat))
		h.ServeHTTP(w, r)
	})
}

func serveHttp(port int) {
	TmplIndex, _ = mustache.ParseFile("views/index.html")
	TmplBlog, _ = mustache.ParseFile("views/blog.html")

	sess, err := mgo.Dial("localhost")
	if err != nil {
		panic(err)
	}
	defer sess.Close()

	c = sess.DB("ks").C("blog")

	/* static file handlers */
	http.Handle("/js/", addCacheHeader(http.FileServer(http.Dir("static/"))))
	http.Handle("/background/", addCacheHeader(http.FileServer(http.Dir("static"))))
	http.Handle("/font/", addCacheHeader(http.FileServer(http.Dir("static"))))

	/* dynamic handler functions */
	http.HandleFunc("/", indexHandler)
	http.HandleFunc("/blog/read/", blogHandler)

	err = http.ListenAndServe(":"+strconv.Itoa(port), nil)
	if err != nil {
		panic(err)
	}
}

func serveSmtp(port int, hostname string) {
	err := smtp.ListenAndServe(":"+strconv.Itoa(port), hostname, receiveMail)
	if err != nil {
		panic(err)
	}
}

func receiveMail(mailItem *smtp.MailItem) error {
	log.Printf("Received mail: %s", mailItem.Plain)

	if mailItem.ToAddress != blogAddress {
		log.Println("Mail authentication failed!")
		return nil
	}

	if mailItem.Plain == "delete" {
		log.Println("Removing blog article ", mailItem.Subject)
		c.RemoveAll(bson.M{"title": mailItem.Subject})
		return nil
	}

	newArticle := &BlogArticle{}

	if mailItem.Html == "" {
		newArticle.Content = mailItem.Plain
	} else {
		newArticle.Content = mailItem.Html
	}

	newArticle.Title = mailItem.Subject
	newArticle.Author = mailItem.FromAddress
	newArticle.Date = time.Now()
	newArticle.HasGallery = false

	if len(mailItem.Attachments) > 0 {
		newArticle.HasGallery = true

		for i, attachment := range mailItem.Attachments {
			newArticle.Gallery = append(newArticle.Gallery, map[string]string{"background_id": attachment.Headers.FileName})
			go newArticle.SaveImages(mailItem.Attachments[i])
		}
	}

	count, err := c.Find(bson.M{"title": mailItem.Subject}).Count()
	if count > 0 {
		err = c.Update(bson.M{"title": mailItem.Subject}, newArticle)
	} else {
		err = c.Insert(newArticle)
	}

	if err != nil {
		log.Println(err)
	}

	return nil
}

func (article *BlogArticle) SaveImages(attachment *smtp.Attachment) {
	image, err := base64.StdEncoding.DecodeString(attachment.Contents)
	if err != nil {
		log.Println("Could not decode image %s", attachment.Headers.FileName)
		return
	}

	ioutil.WriteFile("static/background/"+attachment.Headers.FileName, image, 0664)
}

func (pageList *PageList) Parse(list string) {
	listSplit := strings.Split(list, ", ")
	pageList.PageMap = make(map[string]bool)
	for _, page := range listSplit {
		pageList.PageMap[page] = true
		log.Println("Populating map with key " + page)
	}
}

func (pageList *PageList) Exists(page string) bool {
	if _, ok := pageList.PageMap[page]; ok {
		return true
	}
	return false
}

func configure() {
	var configFile *string = flag.String("config", "gohome.conf", "Path of the configuration file")
	flag.StringVar(&blogAddress, "address", "", "Secret mail address")
	flag.StringVar(&smtpHost, "hostname", "", "SMTP Hostname")
	flag.StringVar(&cdnUrl, "cdn", "", "URL of a CDN network")
	flag.IntVar(&httpPort, "http", 0, "HTTP listening port")
	flag.IntVar(&smtpPort, "smtp", 0, "SMTP listening port")
	flag.Parse()

	config, err := conf.ReadConfigFile(*configFile)
	if err != nil {
		log.Fatal("Could not read config file ", *configFile)
		os.Exit(1)
	}

	if httpPort == 0 {
		httpPort = int(config.GetUint("http.port", 8080))
	}
	if smtpPort == 0 {
		smtpPort = int(config.GetUint("smtp.port", 2525))
	}

	if smtpHost == "" {
		smtpHost = config.GetString("smtp.host", "")
		if smtpHost == "" {
			log.Fatal("No hostname for SMTP banner defined")
		}
	}
	if cdnUrl == "" {
		cdnUrl = config.GetString("cdn.url", "")
	}
	if blogAddress == "" {
		blogAddress = config.GetString("blog.address", "goblog6374@"+smtpHost)
	}

	pageList := config.GetString("pages", "")
	if pageList == "" {
		log.Fatal("No pages defined")
		os.Exit(1)
	}

	pages = &PageList{}
	pages.Parse(pageList)
}

func main() {
	configure()

	wg := &sync.WaitGroup{}
	wg.Add(1)
	go func() {
		log.Printf("HTTP server listening on port ", httpPort)
		serveHttp(httpPort)
		log.Fatal("HTTP server has quit.")
		wg.Done()
	}()

	wg.Add(1)
	go func() {
		log.Printf("SMTP server listening on port ", smtpPort)
		serveSmtp(smtpPort, smtpHost)
		wg.Done()
		log.Fatal("SMTP server has quit.")
	}()

	wg.Wait()
	log.Printf("Exit...")
}
