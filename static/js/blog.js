/*
 * Copyright (c) 2014 Keno Schwalb
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 */

var background = '';
var gallery_shift = null;
var gallery_active = null;
var gallery_active_link = null;

function DCLoaded()
{
	// determine currently active background
	var backgrounds_active = document.getElementsByClassName('background active');
	for(var i = 0; i < backgrounds_active.length; i++)
	{
		background = backgrounds_active[i].id;
	}

	// link click listeners
	var navlinks = document.getElementsByClassName('navlink');
	for(var i = 0; i < navlinks.length; i++)
	{
		navlinks[i].addEventListener('click', function(e)
		{
			e.preventDefault();
			window.location = this.firstElementChild.href;
		});
	}

	var gallery_links = document.getElementsByClassName('gallery_link');
	for(var i = 0; i < gallery_links.length; i++)
	{
		gallery_links[i].addEventListener('click', function(e)
		{
			e.preventDefault();
			var element = this;
			while(element && element.className.indexOf('content') == -1)
			{
				element = element.parentElement;
			}

			if(!gallery_active || (this.getAttribute('data-image') != gallery_active.getAttribute('data-image')))
			{
				gallery_active_link = this;
				if(!gallery_shift)
				{
					if(!mobile)
					{
						hide_gallery(this);
					}
				}
				this.firstElementChild.className = 'active';
				if(gallery_active != null)
				{
					gallery_active.firstElementChild.className = '';
				}
				gallery_active = this;
				set_background(this.getAttribute('data-image'));
			}
			else
			{
				if(mobile)
				{
					var element = gallery_active_link;
					while(element && element.className.indexOf('content') == -1)
					{
						element = element.parentElement;
					}
					gallery_shift = getComputedStyle(element).top;
				}

				show_gallery(this);
			}
		});
	}
}
if(document.readyState != 'loading')
{
	DCLoaded();
}
else
{
	document.addEventListener('DOMContentLoaded', DCLoaded);
}

function hide_gallery(e)
{
	if(typeof e != 'undefined' && e && e.target)
	{
		if(e.relatedTarget.id != 'raster')
		{
			return;
		}
	}

	var element = gallery_active_link;
	while(element && element.className.indexOf('content') == -1)
	{
		element = element.parentElement;
	}

	if(!gallery_shift)
	{
		gallery_shift = getComputedStyle(element).top;
	}

	morpheus(element,
	{
		top: (-1) * (gallery_active_link.parentElement.offsetTop) + 7 - 150 + 'px',
		duration: 300,
		complete: function()
		{
			element.removeEventListener('mouseout', hide_gallery);
			element.addEventListener('mouseover', show_half_gallery);
		}
	});
}

function show_half_gallery(e)
{
	var element = gallery_active_link;
	while(element && element.className.indexOf('content') == -1)
	{
		element = element.parentElement;
	}
	morpheus(element,
	{
		top: (-1) * (gallery_active_link.parentElement.offsetTop) + 7 + 'px',
		duration: 300,
		complete: function()
		{
			element.removeEventListener('mouseover', show_half_gallery);
			element.addEventListener('mouseout', hide_gallery);
		}
	});
}

function show_gallery(animate)
{
	if(typeof animate == 'undefined')
	{
		animate = true;
	}

	var element = gallery_active_link;
	while(element && element.className.indexOf('content') == -1)
	{
		element = element.parentElement;
	}

	if(animate)
	{
		morpheus(element,
		{
			top: gallery_shift,
			duration: 300,
			complete: function()
			{
				element.removeEventListener('mouseout', hide_gallery);
			}
		});
	}
	else
	{
		element.style.top = gallery_shift;
	}
	gallery_active_link.firstElementChild.className = '';
	gallery_active = null;
	gallery_shift = null;
	gallery_active_link = null;
	set_background('blog');
}

function set_background(background_id)
{
	var new_background = document.getElementById('background_' + background_id);
	if(!new_background)
	{
		background_id = 'home';
		new_background = document.getElementById('background_home');
	}

	if('background_' + background_id != background)
	{
		new_background.className = 'background active';
		document.getElementById(background).className = 'background';
		background = 'background_' + background_id;
	}
}
