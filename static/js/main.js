/*
 * Copyright (c) 2014 Keno Schwalb
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 */

var page = ''; // '' so we're not getting any null exceptions
var background = '';
var gallery_shift = null;
var gallery_active = null;
var gallery_active_link = null;

function DCLoaded()
{
	// determine currently active background
	var backgrounds_active = document.getElementsByClassName('background active');
	for(var i = 0; i < backgrounds_active.length; i++)
	{
		background = backgrounds_active[i].id;
	}

	// determine currently active page
	var active_pages = document.getElementsByClassName('navlink active');
	for(i = 0; i < active_pages.length; i++)
	{
		page = active_pages[i].getAttribute('data-page');
	}

	// link click listeners
	var navlinks = document.getElementsByClassName('navlink');
	for(i = 0; i < navlinks.length; i++)
	{
		navlinks[i].addEventListener('click', function(e)
		{
			e.preventDefault();
			switch_page(this.getAttribute("data-page"));
		});
	}

	var gallery_links = document.getElementsByClassName('gallery_link');
	for(i = 0; i < gallery_links.length; i++)
	{
		gallery_links[i].addEventListener('click', function(e)
		{
			e.preventDefault();
			var element = this;
			while(element && element.className.indexOf('content') == -1)
			{
				element = element.parentElement;
			}

			if(!gallery_active || (this.getAttribute('data-image') != gallery_active.getAttribute('data-image')))
			{
				gallery_active_link = this;
				if(!gallery_shift)
				{
					if(!mobile)
					{
						hide_gallery(this);
					}
				}
				this.firstElementChild.className = 'active';
				if(gallery_active !== null)
				{
					gallery_active.firstElementChild.className = '';
				}
				gallery_active = this;
				set_background(this.getAttribute('data-image'));
			}
			else
			{
				if(mobile)
				{
					var element = gallery_active_link;
					while(element && element.className.indexOf('content') == -1)
					{
						element = element.parentElement;
					}
					gallery_shift = getComputedStyle(element).top;
				}

				show_gallery(this);
			}
		});
	}

	window.addEventListener('popstate', function(e)
	{
		if (!e.state)
			return; // workaround for popstate on load
		switch_page(e.state, false);
	});
}
if(document.readyState != 'loading')
{
	DCLoaded();
}
else
{
	document.addEventListener('DOMContentLoaded', DCLoaded);
}

function switch_page(new_page, add_hist_entry)
{
	if(typeof add_hist_entry == 'undefined')
	{
		add_hist_entry = true;
	}

	var link_li = document.querySelector('[data-page="' + new_page + '"]');
	document.getElementById('navlink_' + page).className = 'navlink';
	link_li.className = 'navlink active';
	if(new_page == page)
	{
		if(gallery_active)
		{
			show_gallery(gallery_active);
		}
		return;
	}

	var heading = document.getElementById('heading');
	var new_content = document.getElementById('content_' + new_page);
	var current_content = document.getElementById('content_' + page);
	var obj_hide;
	var obj_show;

	if(page == 'home')
	{
		obj_hide = heading;
	}
	else
	{
		obj_hide = current_content;
	}

	if(new_page == 'home')
	{
		obj_show = heading;
	}
	else
	{
		obj_show = new_content;
	}

	morpheus(obj_hide,
	{
		opacity: 0,
		duration: 300,
		complete: function()
		{
			if(gallery_active)
			{
				show_gallery(gallery_active, false);
			}
			set_background(new_page);
			obj_hide.style.display = 'none';
			obj_show.style.opacity = 0;
			obj_show.style.display = 'block';
			morpheus(obj_show,
			{
				opacity: 1,
				duration: 300
			});
			if(history && add_hist_entry)
			{
				history.pushState(new_page, link_li.firstElementChild.innerHTML, link_li.firstElementChild.href);
			}
			document.title = document.getElementById("website_title").getAttribute("data-title") + " » " + new_page.charAt(0).toUpperCase() + new_page.substr(1, new_page.length);
			track();
		}
	});

	page = new_page;
}

function hide_gallery(e)
{
	if(typeof e != 'undefined' && e && e.target)
	{
		if(e.relatedTarget.id != 'raster')
		{
			return;
		}
	}

	var element = gallery_active_link;
	while(element && element.className.indexOf('content') == -1)
	{
		element = element.parentElement;
	}

	if(!gallery_shift)
	{
		gallery_shift = getComputedStyle(element).top;
	}

	morpheus(element,
	{
		top: (-1) * (gallery_active_link.parentElement.offsetTop) + 7 - 150 + 'px',
		duration: 300,
		complete: function()
		{
			element.removeEventListener('mouseout', hide_gallery);
			element.addEventListener('mouseover', show_half_gallery);
		}
	});
}

function show_half_gallery(e)
{
	var element = gallery_active_link;
	while(element && element.className.indexOf('content') == -1)
	{
		element = element.parentElement;
	}
	morpheus(element,
	{
		top: (-1) * (gallery_active_link.parentElement.offsetTop) + 7 + 'px',
		duration: 300,
		complete: function()
		{
			element.removeEventListener('mouseover', show_half_gallery);
			element.addEventListener('mouseout', hide_gallery);
		}
	});
}

function show_gallery(animate)
{
	if(typeof animate == 'undefined')
	{
		animate = true;
	}

	var element = gallery_active_link;
	while(element && element.className.indexOf('content') == -1)
	{
		element = element.parentElement;
	}

	if(animate)
	{
		morpheus(element,
		{
			top: gallery_shift,
			duration: 300,
			complete: function()
			{
				element.removeEventListener('mouseout', hide_gallery);
			}
		});
	}
	else
	{
		element.style.top = gallery_shift;
	}

	gallery_active_link.firstElementChild.className = '';
	gallery_active = null;
	gallery_shift = null;
	gallery_active_link = null;
	set_background(page);
}

function set_background(background_id)
{
	var new_background = document.getElementById('background_' + background_id);
	if(!new_background)
	{
		background_id = 'home';
		new_background = document.getElementById('background_home');
	}

	if('background_' + background_id != background)
	{
		new_background.className = 'background active';
		document.getElementById(background).className = 'background';
		background = 'background_' + background_id;
	}
}

