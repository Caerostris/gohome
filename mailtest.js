/*
 * GoHome - A minimalist webserver for a personal homepage with a blog
 *
 * Copyright (c) 2014 Keno Schwalb
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 */

var nodemailer = require('nodemailer');
var smtpTransport = require('nodemailer-smtp-transport');
var transporter = nodemailer.createTransport();

var mailOptions = {
	from: 'Agent Smith <agent@smith.com>',
	to: 'goblog6374@yourserver.com',
	subject: 'We need to talk',
	text: 'Hello, Mr Anderson.',
	html: '<b>Hello, Mr Anderson.</b>'
};

transporter.sendMail(mailOptions);
